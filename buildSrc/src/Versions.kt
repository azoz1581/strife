package com.serebit.strife.gradle

/** Versions of dependencies for type-safe consistency. */
object Versions {
    const val COROUTINES = "1.3.0-RC2"
    const val KTOR = "1.2.3"
    const val SERIALIZATION = "0.11.1"
    const val LOGKAT = "0.4.6"
    const val KLOCK = "1.5.0"
}
